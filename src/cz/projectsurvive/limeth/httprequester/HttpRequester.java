package cz.projectsurvive.limeth.httprequester;

import com.google.common.base.Joiner;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by limeth on 14.8.15.
 */
public class HttpRequester extends JavaPlugin
{
    public static final String PERMISSION_COMMAND_HTTP = "HttpRequester.command.http";
    public static final String CHARSET = java.nio.charset.StandardCharsets.UTF_8.name();

    @Override
    public void onEnable()
    {
        Bukkit.getPluginCommand("http").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        if(!sender.hasPermission(PERMISSION_COMMAND_HTTP))
        {
            sender.sendMessage(ChatColor.RED + "No permission. (" + PERMISSION_COMMAND_HTTP + ")");
            return false;
        }

        if(args.length < 2)
        {
            sender.sendMessage("/" + label + " [get|post] [url]");
            return true;
        }

        RequestType type;

        try
        {
            type = RequestType.valueOf(args[0].toUpperCase());
        }
        catch(IllegalArgumentException e)
        {
            sender.sendMessage(ChatColor.RED + "Invalid request type " + args[0].toUpperCase() + ". Available request types: " + Arrays.toString(RequestType.values()));
            return false;
        }

        String url = args[1];

        for(int i = 2; i < args.length; i++)
            url += " " + args[i];

        String[] urlParts = url.split("\\?", 2);
        String path = urlParts[0];
        String query = urlParts.length > 1 ? urlParts[1] : null;
        String response = null;

        try
        {
            response = type.request(path, query);
        }
        catch(IOException e)
        {
            sender.sendMessage(ChatColor.RED + "An error occurred while executing the command: " + e.getLocalizedMessage());
            e.printStackTrace();
            return false;
        }

        if(response != null && response.length() > 0)
            sender.sendMessage(response);

        return true;
    }
}
