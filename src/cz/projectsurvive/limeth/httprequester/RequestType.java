package cz.projectsurvive.limeth.httprequester;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by limeth on 14.8.15.
 */
public enum RequestType
{
    GET
            {
                @Override
                public String request(String path, String query) throws IOException
                {
                    String url = path + (query != null ? ("?" + query) : "");
                    URLConnection connection = new URL(url).openConnection();
                    connection.setConnectTimeout(10000);
                    connection.setRequestProperty("Accept-Charset", HttpRequester.CHARSET);

                    return readResponse(connection);
                }
            },
    POST
            {
                @Override
                public String request(String path, String query) throws IOException
                {
                    URLConnection connection = new URL(path).openConnection();
                    connection.setConnectTimeout(10000);
                    connection.setRequestProperty("Accept-Charset", HttpRequester.CHARSET);
                    connection.setRequestProperty("Content-Type",
                                                  "application/x-www-form-urlencoded;charset=" + HttpRequester.CHARSET);

                    if(query != null)
                    {
                        connection.setDoOutput(true);

                        OutputStream output = connection.getOutputStream();

                        output.write(query.getBytes(HttpRequester.CHARSET));
                        output.flush();
                        output.close();
                    }
                    else
                    {
                        ((HttpURLConnection) connection).setRequestMethod("POST");
                    }

                    return readResponse(connection);
                }
            };

    private static String readResponse(URLConnection connection) throws IOException
    {
        InputStream response = connection.getInputStream();
        String contentType = connection.getHeaderField("Content-Type");
        String charset = null;

        for(String param : contentType.replace(" ", "").split(";"))
        {
            if(param.startsWith("charset="))
            {
                charset = param.split("=", 2)[1];
                break;
            }
        }

        if(charset == null)
            charset = HttpRequester.CHARSET;

        StringBuilder result = new StringBuilder();

        if(charset != null)
        {
            try(BufferedReader reader = new BufferedReader(new InputStreamReader(response, charset)))
            {
                boolean first = true;

                for(String line; (line = reader.readLine()) != null; )
                {
                    if(first)
                        first = false;
                    else
                        result.append('\n');

                    result.append(line);
                }
            }
        }

        return result.toString();
    }

    public abstract String request(String path, String query) throws IOException;
}
